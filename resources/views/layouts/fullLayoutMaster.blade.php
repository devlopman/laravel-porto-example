<!DOCTYPE html>
<html lang="en">
    <!--begin::Head-->
    <head>
        <title>@yield('title')</title>
    </head>
    <!--end::Head-->
    <!--begin::Body-->
    <body id="kt_body" class="bg-body">
        @yield('content')
    </body>
    <!--end::Body-->
</html>
