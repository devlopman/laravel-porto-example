<?php

namespace App\Containers\Example\UI\WEB\Controllers;

use App\Ship\Abstracts\Controllers\Controller;
use Illuminate\Contracts\View\View;

class ExampleController extends Controller
{
    /**
     * @return View
     */
    public function render()
    {
        return view('Example/UI/WEB/Resources/Pages/example');
    }
}
