<?php

namespace App\Containers\Example\UI\WEB\Routes;

use Illuminate\Support\Facades\Route;
use Illuminate\Support\ServiceProvider;
use App\Containers\Example\UI\WEB\Controllers\ExampleController;

class RouteProvider extends ServiceProvider
{
    public function boot(): void
    {
        $this->authWebRoute();
    }

    private function authWebRoute(): void
    {

    Route::group([

    ], function () {
        Route::get(
            '/',
            [
                'as' => 'example',
                'uses' => ExampleController::class . '@render',
            ]
        );
    });
    }
}
