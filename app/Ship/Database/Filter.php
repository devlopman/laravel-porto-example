<?php

namespace App\Ship\Database;

use Jenssegers\Mongodb\Eloquent\Builder as EloquentBuilder;

abstract class Filter
{
    /**
     * @param EloquentBuilder $query
     *
     * @return EloquentBuilder
     */
    abstract public function addToQuery(EloquentBuilder $query): EloquentBuilder;
}
