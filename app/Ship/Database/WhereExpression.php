<?php

namespace App\Ship\Database;

use Jenssegers\Mongodb\Eloquent\Builder as EloquentBuilder;

class WhereExpression extends Filter
{
    /** @var string */
    private string $field;
    /** @var string */
    private string $symbol;
    /** @var mixed */
    private $value;

    /**
     * @param string $field
     * @param string $symbol
     * @param mixed $value
     */
    public function __construct(string $field, string $symbol, $value)
    {
        $this->field = $field;
        $this->symbol = $symbol;
        $this->value = $value;
    }

    /**
     * @param EloquentBuilder $query
     *
     * @return EloquentBuilder
     */
    public function addToQuery(EloquentBuilder $query): EloquentBuilder
    {
        return $query->where($this->field, $this->symbol, $this->value);
    }
}
