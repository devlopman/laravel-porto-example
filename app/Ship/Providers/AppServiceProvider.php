<?php

namespace App\Ship\Providers;

use App\Ship\Abstracts\HttpClient\HttpClientInterface;
use App\Ship\Http\Guzzle\GuzzleHttpClient;
use Carbon\Carbon;
use Illuminate\Filesystem\Filesystem;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\File;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    public array $bindings = [
        HttpClientInterface::class => GuzzleHttpClient::class,
    ];

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register(): void
    {
        $filesystem = new Filesystem();

        foreach ($filesystem->directories(app_path('/Containers')) as $directory) {
            $configsDir = $directory . '/Configs';
            if (is_dir($configsDir)) {
                $files = File::allFiles($configsDir);
                foreach ($files as $file) {
                    $fileNameOnly = str_replace('.php', '', $file->getFilename());
                    $this->mergeConfigFrom($file->getPathname(), $fileNameOnly);
                }
            }
        }
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        \URL::forceScheme('https');

        Carbon::setLocale(config('app.locale'));

        $migrationsDirsList = Cache::get('migrationsDirsList', function () {
            $filesystem = new Filesystem();
            $migrationsDirsList = [];

            foreach ($filesystem->directories(app_path('/Containers')) as $directory) {
                $migrationsDir = $directory . '/Data/Migrations';
                if (is_dir($migrationsDir)) {
                    $migrationsDirsList[] = $migrationsDir;
                }
            }
            Cache::put('migrationsDirsList', $migrationsDirsList, 120);
            return $migrationsDirsList;
        });

        $this->loadMigrationsFrom($migrationsDirsList);
    }
}
