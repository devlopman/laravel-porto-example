<?php

namespace App\Ship\Exceptions;

use App\Ship\Abstracts\Exceptions\Exception as AbstractException;

/**
 * Class Exception.
 */
abstract class Exception extends AbstractException
{

}
