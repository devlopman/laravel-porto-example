<?php

namespace App\Ship\Abstracts\Models;

use Jenssegers\Mongodb\Eloquent\Model as LaravelEloquentModel;

/**
 * Class Model.
 */
abstract class Model extends LaravelEloquentModel
{

}
