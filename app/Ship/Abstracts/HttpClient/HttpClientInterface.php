<?php
namespace App\Ship\Abstracts\HttpClient;

/**
 * Interface HttpClient
 * @package App\Interfaces
 */
interface HttpClientInterface {

    /**
     * @param string $url
     * @param array $options
     */
    public function post(string $url, array $options = []);
    public function get(string $url, array $options = []);
}
