<?php


namespace App\Ship\Abstracts\Collections;


use Spatie\DataTransferObject\DataTransferObjectCollection;

abstract class Collection extends DataTransferObjectCollection
{

}
