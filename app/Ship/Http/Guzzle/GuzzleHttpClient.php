<?php
namespace App\Ship\Http\Guzzle;

use App\Ship\Abstracts\HttpClient\HttpClientInterface;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Exception\RequestException;
use Psr\Http\Message\ResponseInterface;


/**
 * The basic class for working with post and get requests
 * Class GuzzleHttpClient
 * @package app\Classes
 * return ResponseInterface
 */
class GuzzleHttpClient implements HttpClientInterface
{

    protected Client $client;

    public function __construct(Client $client)
    {
        $this->client = $client;
    }

    /**
     * @param string $url
     * @param array $options
     * @return ResponseInterface
     * @throws GuzzleException
     */
    public function post(string $url, array $options = []): ResponseInterface
    {
        try {
            return $this->client->post($url, $options);

        } catch (RequestException $e) {
            //throw new InvalidRequestException($e->getCode() . ":" . strip_tags($e->getMessage()));
        }
    }

    /**
     * @param string $url
     * @param array $options
     * @return ResponseInterface
     * @throws GuzzleException
     */
    public function get(string $url, array $options = []): ResponseInterface
    {
        try {
            return $this->client->get($url, $options);

        } catch (RequestException $e) {
            //throw new InvalidRequestException($e->getCode() . ":" . strip_tags($e->getMessage()));
        }
    }

}
