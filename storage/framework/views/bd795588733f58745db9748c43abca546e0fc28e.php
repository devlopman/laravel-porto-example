<!DOCTYPE html>
<html lang="en">
    <!--begin::Head-->
    <head>
        <title><?php echo $__env->yieldContent('title'); ?></title>
        <meta name="description" content="" />
        <meta name="keywords" content="" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <meta charset="utf-8" />
        <meta property="og:locale" content="en_US" />
        <meta property="og:type" content="article" />
        <meta property="og:title" content="<?php echo $__env->yieldContent('title'); ?>" />
        <meta property="og:url" content="" />
        <meta property="og:site_name" content="" />
        <link rel="canonical" href="https://preview.keenthemes.com/metronic8" />
        <link rel="shortcut icon" href="" />
        <!--begin::Fonts-->
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" />
        <!--end::Fonts-->
        <!--begin::Global Stylesheets Bundle(used by all pages)-->
        <link href="plugins/global/plugins.bundle.css" rel="stylesheet" type="text/css" />
        <link href="css/style.bundle.css" rel="stylesheet" type="text/css" />
        <!--end::Global Stylesheets Bundle-->
    </head>
    <!--end::Head-->
    <!--begin::Body-->
    <body id="kt_body" class="bg-body">
        <?php echo $__env->yieldContent('content'); ?>
    </body>
    <!--end::Body-->
</html>
<?php /**PATH /var/www/b2b.web-zaim.ru/back-office/resources/views/layouts/auth.blade.php ENDPATH**/ ?>