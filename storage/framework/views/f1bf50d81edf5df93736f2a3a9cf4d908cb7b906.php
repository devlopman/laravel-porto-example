

<?php $__env->startSection('content'); ?>

    <!--begin::Main-->
    <?php if(theme()->getOption('layout', 'main/type') === 'blank'): ?>
        <div class="d-flex flex-column flex-root">
            <?php echo e($slot); ?>

        </div>
    <?php else: ?>
        <!--begin::Root-->
        <div class="d-flex flex-column flex-root">
            <!--begin::Page-->
            <div class="page d-flex flex-row flex-column-fluid">
            <?php echo e(theme()->getView('layout/aside/_base')); ?>


            <!--begin::Wrapper-->
                <div class="wrapper d-flex flex-column flex-row-fluid" id="kt_wrapper">
                <?php echo e(theme()->getView('layout/header/_base')); ?>


                <!--begin::Content-->
                    <div class="content d-flex flex-column flex-column-fluid" id="kt_content">
                        <?php echo e(theme()->getView('layout/_content', compact('slot'))); ?>

                    </div>
                    <!--end::Content-->

                    <?php echo e(theme()->getView('layout/_footer')); ?>

                </div>
                <!--end::Wrapper-->

                <?php if(theme()->getOption('layout', 'sidebar/display') === true): ?>
                    <?php echo e(theme()->getView('layout/sidebar/_base')); ?>

                <?php endif; ?>
            </div>
            <!--end::Page-->
        </div>
        <!--end::Root-->

        <!--begin::Drawers-->
        <?php echo e(theme()->getView('partials/topbar/_activity-drawer')); ?>

        <?php echo e(theme()->getView('partials/explore/_main')); ?>

        <!--end::Drawers-->

        <?php if(theme()->getOption('layout', 'scrolltop/display') === true): ?>
            <?php echo e(theme()->getView('layout/_scrolltop')); ?>

        <?php endif; ?>
    <?php endif; ?>
    <!--end::Main-->

<?php $__env->stopSection(); ?>

<?php echo $__env->make('base.base', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /var/www/b2b.web-zaim.ru/back-office/resources/views/layouts/master.blade.php ENDPATH**/ ?>