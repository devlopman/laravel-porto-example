<!DOCTYPE html>
<html lang="en">
    <!--begin::Head-->
    <head>
        <title><?php echo $__env->yieldContent('title'); ?></title>
    </head>
    <!--end::Head-->
    <!--begin::Body-->
    <body id="kt_body" class="bg-body">
        <?php echo $__env->yieldContent('content'); ?>
    </body>
    <!--end::Body-->
</html>
<?php /**PATH /var/www/b2b.web-zaim.ru/back-office/resources/views/layouts/fullLayoutMaster.blade.php ENDPATH**/ ?>