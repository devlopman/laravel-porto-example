<?php

return [
    'oracle_persistent' => '',
    'oracle_connection_type' => '',
    'environment' => '',
    'oracle_instance' => "",
    'oracle_use_env_dsn' => 1,
    'oracle_host' => "",
    'oracle_port' => 1521,
    'oracle_sid' => "",
    'oracle_user' => "",
    'oracle_password' => ""
];
