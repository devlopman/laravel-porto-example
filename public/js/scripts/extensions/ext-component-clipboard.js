'use strict';

let container = document.querySelectorAll(".btn-copy");
let text_container = document.querySelectorAll(".copy-to-clipboard-input");


text_container.forEach(function(item, i) {
    $(item).addClass('cb-target-' + i);
});

container.forEach(function(item, i) {
    $(item).attr('data-clipboard-target','.cb-target-' + i);
    item.addEventListener('click', function (e) {
        copyText(item)
    })
});

function copyText(e) {
    let item = $(e).attr('data-clipboard-target');
    item = document.querySelector(item);
    navigator.clipboard.writeText(item.value)
        .then(() => {
            toastr['success']('', 'Скопировано в буфер обмена');
        })
        .catch(err => {
            console.log('Ошибка', err);
        });
}

