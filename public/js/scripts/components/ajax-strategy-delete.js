$(function () {
    'use strict';

    let confirmText = $('#confirm-text');
    let confirmColor = $('#confirm-color');

    $('.delete-button').click(function (event) {
        let id = $(event.target).attr('data-id');
        Swal.fire({
            title: 'Вы уверены?',
            text: "Вы не сможете отменить это!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Да, удалить!',
            cancelButtonText: 'ОТменить',
            customClass: {
                confirmButton: 'btn btn-primary',
                cancelButton: 'btn btn-outline-danger ml-1'
            },
            buttonsStyling: false
        }).then(function (result) {
            if (result.value) {
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                $.ajax(
                    {
                        url: "/strategy/delete/" + id,
                        type: 'delete',
                        data: {
                            "id": id
                        },
                        success: function (response) {
                            Swal.fire({
                                icon: 'success',
                                title: 'Удалено!',
                                text: 'Запись была успешно удалена.',
                                customClass: {
                                    confirmButton: 'btn btn-success'
                                }
                            }).then(function (result) {
                                if (result.value) {
                                    document.location.reload();
                                }
                            });
                            console.log(response);
                        },
                        error: function (xhr) {
                            console.log(xhr.responseText);
                        }
                });
            }
        });
    });
})


