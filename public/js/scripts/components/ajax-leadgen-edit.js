$('.edit-button').click(function (event) {
    let id = $(event.target).attr('data-id');
    let card = document.querySelector('.leadgen-card');
    let title = document.querySelector('.leadgen-add-title');
    card.setAttribute("style", "border: 3px solid #FF9F43;");
    title.innerHTML = 'Редактирование шаблона лидогенератора';
    $.ajax({
        url: "/leadgen/edit/" + id,
        method: "get"
    }).done(function(response) {
        $("input[name='leadgen_name']").val(response.leadgen_name);
        $("input[name='postback_lag_time']").val(response.postback_lag_time);
        $("textarea[name='bid_loan_link']").val(response.bid_loan_link);
        $("textarea[name='denied_loan_link']").val(response.denied_loan_link);
        $("textarea[name='success_loan_link']").val(response.success_loan_link);
        if (response.do_not_send_bid === true) {
            $("input[name='do_not_send_bid']").prop('checked', true);
        } else {
            $("input[name='do_not_send_bid']").prop('checked', false);
        }
        if (response.do_not_send_denied === true) {
            $("input[name='do_not_send_denied']").prop('checked', true);
        } else {
            $("input[name='do_not_send_denied']").prop('checked', false);
        }
        if (response.do_not_send_success === true) {
            $("input[name='do_not_send_success']").prop('checked', true);
        } else {
            $("input[name='do_not_send_success']").prop('checked', false);
        }
        if (response.is_rbl === true) {
            $("input[name='is_rbl']").val(response.is_rbl).prop('checked', true);
        } else {
            $("input[name='is_rbl']").val(response.is_rbl).prop('checked', false);
        }
        if (response.is_postback_repeatable === true) {
            $("input[name='is_postback_repeatable']").prop('checked', true);
        } else {
            $("input[name='is_postback_repeatable']").prop('checked', false);
        }
        $("input[name='rbl_param_name']").val(response.rbl_param_name);

        let rblArray = response.rbl;
        rblArray.forEach(function(item, i) {
            if (i !== rblArray.length - 1) {
                $("button[name='add-rbl']").click();
            }
            $("input[name='rbl[" + i + "][rbl_price]']").val(item.rbl_price);
            $("input[name='rbl[" + i + "][rbl_param_value]']").val(item.rbl_param_value);
            $("input[name='rbl[" + i + "][_id]']").val(item._id);
        });

        $(".add-leadgen").text('Отредактировать').attr('type', 'button');
        $("#leadgen-form").attr("action","/leadgen/update/"+id).append("<input type='hidden' name='_method' value='PUT'/>")

        $('.add-leadgen').click(function (event) {
            let formData = $("#leadgen-form").serializeArray();
            let url = "/leadgen/update/" + id;
            let not_send_bid = $("input[name='do_not_send_bid']").prop('checked');
            let do_not_send_denied = $("input[name='do_not_send_denied']").prop('checked');
            let do_not_send_success = $("input[name='do_not_send_success']").prop('checked');
            let is_rbl = $("input[name='is_rbl']").prop('checked');
            let is_postback_repeatable = $("input[name='is_postback_repeatable']").prop('checked');

            formData[formData.length] = { name: "id", value: id };

            if (not_send_bid === false) {
                formData[formData.length] = { name: "do_not_send_bid", value: null };
            }
            if (do_not_send_denied === false) {
                formData[formData.length] = { name: "do_not_send_denied", value: null };
            }
            if (do_not_send_success === false) {
                formData[formData.length] = { name: "do_not_send_success", value: null };
            }
            if (is_rbl === false) {
                formData[formData.length] = { name: "is_rbl", value: null };
            } else {
                formData[formData.length] = { name: "is_rbl", value: 'on' };
            }
            if (is_postback_repeatable === false) {
                formData[formData.length] = { name: "is_postback_repeatable", value: null };
            } else {
                formData[formData.length] = { name: "is_postback_repeatable", value: 'on' };
            }

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax(
                {
                    url: url,
                    type: 'put',
                    data: formData,
                    success: function (response) {
                        Swal.fire({
                            icon: 'success',
                            title: 'Отредактировано!',
                            text: 'Запись была успешно изменена.',
                            customClass: {
                                confirmButton: 'btn btn-success'
                            }
                        }).then(function (result) {
                            if (result.value) {
                                document.location.reload();
                            }
                        });
                        console.log(response);
                    },
                    error: function (xhr) {
                        if (xhr.status === 422) {
                            Swal.fire({
                                icon: 'error',
                                title: 'Ошибка валидации!',
                                text: 'Заполните обязательные поля.',
                                customClass: {
                                    confirmButton: 'btn btn-error'
                                }
                            })
                        }
                        console.log(xhr.responseText);
                    }
                });
            });
        })
});

