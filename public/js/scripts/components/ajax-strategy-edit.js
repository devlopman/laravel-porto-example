$('.edit-button').click(function (event) {
    let id = $(event.target).attr('data-id')

    $.get('/strategy/edit/' + id, function(data) {
        $('.strategy-wrapper').empty().append(data);
        if (feather) {
            feather.replace({ width: 14, height: 14 });
        }
        $('.invoice-repeater, .repeater-default').repeater({
            show: function () {
                $(this).slideDown();
            },
            hide: function (deleteElement) {
                $(this).slideUp(deleteElement);
            }
        });
    });
});

