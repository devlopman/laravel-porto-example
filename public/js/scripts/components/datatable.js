$.ajaxSetup ({
    headers: {
        'X-CSRF-TOKEN': $ ('meta[name="csrf-token"]').attr ('content')
    }
});

let form = function (d) {
    d.demand_id = $('input[name=demand_id]').val();
    d.utm_content = $('input[name=utm_content]').val();
    d.source = $('input[name=source]').val();
    d.utm_medium = $('input[name=utm_medium]').val();
    d.client_type = $('select[name=client_type]').val();
    d.dt_date = $('input[name=dt_date]').val();
    d.value_from_start_date = $('input[name=value_from_start_date]').val();
    d.value_from_end_date = $('input[name=value_from_end_date]').val();
    d.utm_term = $('input[name=utm_term]').val();
    d.utm_campaign = $('input[name=utm_campaign]').val();
    d.send_status = $('select[name=send_status]').val();
    d.demand_status = $('select[name=demand_status]').val();
};

var postsTable = $ ('#reporttable').DataTable ({
    "ordering": false,
    "processing": true,
    "serverSide": true,
    "ajax": {
        "url": "/reports/data",
        "dataType": "json",
        "type": "POST",
        data: form
    },
    "bFilter": false,
    "dom": '<"d-flex justify-content-between align-items-center mx-0 row"<"col-sm-12 col-md-6"l><"col-sm-12 col-md-6"f>>t<"d-flex justify-content-between mx-0 row"<"col-sm-12 col-md-6"i><"col-sm-12 col-md-6"p>>',
    "columns": [
        {"data": "client.id", "defaultContent": "-"},
        {"data": "client.registered", "defaultContent": "-"},
        {"data": "client.esia", "defaultContent": "-"},
        {"data": "demand.id", "defaultContent": "-"},
        {"data": function (data) {
                let date =  new Date(data.demand.timestamp*1000)
                return date.toLocaleString()
            }, "defaultContent": "-"},
        {"data": "demand.count", "defaultContent": "-"},
        {"data": "class", "defaultContent": "-"},
        {"data": function (data) {
                return data.demands.status+ '/'+ data.demands.stage
            }
        },
        {"data": "source"},
        {"data": "lead_query.utm_medium", "defaultContent": "-"},
        {"data": "lead_query.utm_campaign", "defaultContent": "-"},
        {"data": "lead_query.utm_content", "defaultContent": "-"},
        {"data": "lead_query.utm_term", "defaultContent": "-"},
        {"data": "lead_query.utm_referrer", "defaultContent": "-"},
        {"data": "lead_query.click_id", "defaultContent": "-"},
        {"data": "lead_query.http_referrer", "defaultContent": "-"},
        {"data": "demands.loan_amount", "defaultContent": "-"},
        {"data": "demands.recommended_amount", "defaultContent": "-"},
        {"data": "demands.rbl_score", "defaultContent": "-"},
        {"data": "demand.product.id", "defaultContent": "-"},
        {"data": "strategies.strategy.name", "defaultContent": "-"},
        {"data": "strategies.leadgen_price", "defaultContent": "-"},
        {"data": "demands.send_status", "defaultContent": "-"},
        {"data": "demands.updated_at", "defaultContent": "-"}
    ],
    "order": [[1, "desc"]],
    "language": {
        "processing": '<div class="spinner-grow text-success mr-1" role="status"><span class="sr-only">Loading...</span></div>',
        "zeroRecords": "Ничего не найдено",
        "paginate": {
            "previous": '&nbsp;',
            "next": '&nbsp;'
        },
        "lengthMenu": "Показать _MENU_ записей",
        "info": "Записи с _START_ до _END_ из _TOTAL_ записей",
        "infoEmpty": "Записи с 0 до 0 из 0 записей",
        "infoFiltered": "(отфильтровано из _MAX_ записей)",
        "loadingRecords": "Загрузка записей...",
        "emptyTable": "В таблице отсутствуют данные"
    },
    "lengthMenu": [[50, 100, 250, 500], [50, 100, 250, 500]],
    "stateSave": true
});

$ ('.data-table-search-input-text').on ('keyup change', function () {
    var inputElement = $ (this);

    ojoDelay (function () {

        var i = inputElement.attr ('data-column');
        var v = inputElement.val ();
        postsTable.api ().columns (i).search (v).draw ();
        //postsTable.api ().ajax.reload ();
    }, 1000, this);
});

ojoDelay = (function () {
    var timer = 0;
    return function (callback, ms, that) {
        clearTimeout (timer);
        timer = setTimeout (callback.bind (that), ms);
    };
}) ();

$("#report-filter").submit(function(e) {
    var form = $(this);
    var url = form.attr('action');

    postsTable.draw();
    postsTable.ajax.reload();
    e.preventDefault();
});

// Datepicker for advanced filter
var separator = ' - ',
    rangePickr = $('.flatpickr-range'),
    dateFormat = 'MM/DD/YYYY';
var options = {
    autoUpdateInput: false,
    autoApply: true,
    locale: {
        format: dateFormat,
        separator: separator
    },
    opens: $('html').attr('data-textdirection') === 'rtl' ? 'left' : 'right'
};

//
if (rangePickr.length) {
    rangePickr.flatpickr({
        mode: 'range',
        dateFormat: 'm/d/Y',
        onClose: function (selectedDates, dateStr, instance) {
            var startDate = '',
                endDate = new Date();
            if (selectedDates[0] != undefined) {
                startDate =
                    selectedDates[0].getMonth() + 1 + '/' + selectedDates[0].getDate() + '/' + selectedDates[0].getFullYear();
                $('.start_date').val(startDate);
            }
            if (selectedDates[1] != undefined) {
                endDate =
                    selectedDates[1].getMonth() + 1 + '/' + selectedDates[1].getDate() + '/' + selectedDates[1].getFullYear();
                $('.end_date').val(endDate);
            }
            $(rangePickr).trigger('change').trigger('keyup');
        }
    });
}

var filterByDate = function (column, startDate, endDate) {
    $.fn.dataTableExt.afnFiltering.push(function (oSettings, aData, iDataIndex) {
        var rowDate = normalizeDate(aData[column]),
            start = normalizeDate(startDate),
            end = normalizeDate(endDate);

        if (start <= rowDate && rowDate <= end) {
            return true;
        } else if (rowDate >= start && end === '' && start !== '') {
            return true;
        } else if (rowDate <= end && start === '' && end !== '') {
            return true;
        } else {
            return false;
        }
    });
};

var normalizeDate = function (dateString) {
    var date = new Date(dateString);
    var normalized =
        date.getFullYear() + '' + ('0' + (date.getMonth() + 1)).slice(-2) + '' + ('0' + date.getDate()).slice(-2);
    return normalized;
};

/*
let filtered = $.ajax({
        type: "GET",
        url: '/reports/count'
    });
*/

$(".create-excel").click(function (e) {
    Swal.fire({
        title: 'Создание Excel отчета',
        text: "Будет создан отчет из текущей фильтрации таблицы",
        icon: 'info',
        showCancelButton: true,
        confirmButtonText: 'Создать',
        cancelButtonText: 'Отменить',
        customClass: {
            confirmButton: 'btn btn-success',
            cancelButton: 'btn btn-outline-warning ml-1'
        },
        buttonsStyling: false
    }).then(function (result) {
        if (result.value) {
            $.ajax({
                type: "POST",
                url: '/reports/excel',
                data: $("#report-filter").serialize(),
                success: function (response) {
                    Swal.fire({
                        icon: 'success',
                        title: 'Успешно!',
                        text: 'Задача успешно создана. Перейти на страницу со списком отчетов?',
                        showCancelButton: true,
                        confirmButtonText: 'Да',
                        cancelButtonText: 'Нет',
                        customClass: {
                            confirmButton: 'btn btn-success',
                            cancelButton: 'btn btn-outline-warning ml-1'
                        }
                    }).then(function (result) {
                        if (result.value) {
                            window.location.href = '/reports/list';
                        }
                    });
                },
                error: function (xhr) {
                    console.log(xhr.responseText);
                }
            },
            );
        }
    })
});

