/*=========================================================================================
    File Name: form-repeater.js
    Description: form repeater page specific js
==========================================================================================*/

$(function () {
  'use strict';

  // form repeater jquery
  $('.repeater').repeater({
    initEmpty: false,
    defaultValues: {
      'text-input': 'foo'
    },
    show: function () {
      $(this).slideDown();
        if ($(".select2")[0]) {
            $('.select2-container').remove();
            $('select').select2({
                width: '100%',
                placeholder: "Выберите из списка",
            });
        }
      // Feather Icons
      if (feather) {
        feather.replace({ width: 14, height: 14 });
      }
    },
    hide: function (deleteElement) {
        $(this).slideUp(deleteElement);
    },
      ready: function (setIndexes) {
      },
      isFirstItemUndeletable: true
  });
});
